unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IdMessage, Vcl.StdCtrls,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP,
  Vcl.ExtCtrls, System.Actions, Vcl.ActnList, IdIOHandler, IdIOHandlerSocket,
  IdIOHandlerStack, IdSSL, IdSSLOpenSSL;

type
  TfrmSMTP = class(TForm)
    pnlSMTP: TPanel;
    SMTP: TIdSMTP;
    Label1: TLabel;
    Label2: TLabel;
    Username: TLabel;
    Password: TLabel;
    edtHost: TEdit;
    edtPassword: TEdit;
    edtUsername: TEdit;
    cmbPort: TComboBox;
    rgSecurity: TRadioGroup;
    edtFrom: TEdit;
    Label3: TLabel;
    Label6: TLabel;
    edtSubject: TEdit;
    Label4: TLabel;
    edtTo: TEdit;
    memMessage: TMemo;
    Button1: TButton;
    Actions: TActionList;
    acSend: TAction;
    memLog: TMemo;
    SSL: TIdSSLIOHandlerSocketOpenSSL;
    procedure acSendExecute(Sender: TObject);
    procedure SMTPConnected(Sender: TObject);
    procedure SMTPDisconnected(Sender: TObject);
    procedure SMTPStatus(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure SMTPTLSNotAvailable(Asender: TObject; var VContinue: Boolean);
    procedure SMTPFailedRecipient(Sender: TObject; const AAddress, ACode,
      AText: string; var VContinue: Boolean);
    procedure SSLStatus(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure SSLStatusInfo(const AMsg: string);
  private
    procedure Log(Text: string; Args: array of const);
  public
    { Public declarations }
  end;

var
  frmSMTP: TfrmSMTP;

implementation

{$R *.dfm}

procedure TfrmSMTP.acSendExecute(Sender: TObject);
var
  EMail: TIdMessage;
begin
  try
    SMTP.Host := edtHost.Text;
    SMTP.Port := StrToInt(cmbPort.Text);
    SMTP.Username := edtUsername.Text;
    SMTP.Password := edtPassword.Text;
    SMTP.UseTLS := TIdUseTLS(rgSecurity.ItemIndex);
    Log('Connecting to SMTP server: %s:%d', [SMTP.Host, SMTP.Port]);
    Log('  Account: %s', [SMTP.Username]);
    Log('  Security level: ', [rgSecurity.Items[Ord(SMTP.UseTLS)]]);
    SMTP.Connect;
    try
      EMail := TIdMessage.Create;
      try
        EMail.From.Address := edtFrom.Text;
        EMail.Recipients.Add.Address := edtTo.Text;
        EMail.Subject := edtSubject.Text;
        EMail.Body.Assign(memMessage.Lines);

        Log('Sending message to %s', [edtTo.Text]);
        SMTP.Send(EMail);
        Log('Message sent.', [edtTo.Text]);
      finally
        EMail.Free;
      end;
    finally
      SMTP.DisconnectNotifyPeer;
    end;
  except
    on E:Exception do
      Log('EXCEPTION: %s', [E.Message]);
  end;
end;

procedure TfrmSMTP.Log(Text: string; Args: array of const);
begin
  memLog.Lines.Add(Format(Text, Args));
end;

procedure TfrmSMTP.SMTPConnected(Sender: TObject);
begin
  Log('Connected to SMTP server.', []);
end;

procedure TfrmSMTP.SMTPDisconnected(Sender: TObject);
begin
  Log('Disconnected from SMTP server.', []);
end;

procedure TfrmSMTP.SMTPFailedRecipient(Sender: TObject; const AAddress, ACode, AText: string; var VContinue: Boolean);
begin
  Log('Recipient "%s" not found. Code: %d (%s)', [AAddress, ACode, AText]);
  VContinue := True;
end;

procedure TfrmSMTP.SMTPStatus(ASender: TObject; const AStatus: TIdStatus; const AStatusText: string);
begin
  Log('  :: Current status: %s', [AStatusText]);
end;

procedure TfrmSMTP.SMTPTLSNotAvailable(Asender: TObject; var VContinue: Boolean);
begin
  Log('TLS not available.', []);
  VContinue := True;
end;

procedure TfrmSMTP.SSLStatus(ASender: TObject; const AStatus: TIdStatus; const AStatusText: string);
begin
  Log('  :: Current SSL status: %s', [AStatusText]);
end;

procedure TfrmSMTP.SSLStatusInfo(const AMsg: string);
begin
  Log('  :: Current SSL status info: %s', [AMsg]);
end;

end.
