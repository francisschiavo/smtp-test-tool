object frmSMTP: TfrmSMTP
  Left = 0
  Top = 0
  Caption = 'SMTP Test tool'
  ClientHeight = 580
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pnlSMTP: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 422
    Height = 574
    Align = alLeft
    TabOrder = 0
    ExplicitLeft = -117
    ExplicitTop = -29
    ExplicitHeight = 573
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 22
      Height = 13
      Caption = 'Host'
    end
    object Label2: TLabel
      Left = 304
      Top = 5
      Width = 20
      Height = 13
      Caption = 'Port'
    end
    object Username: TLabel
      Left = 8
      Top = 48
      Width = 48
      Height = 13
      Caption = 'Username'
    end
    object Password: TLabel
      Left = 216
      Top = 48
      Width = 46
      Height = 13
      Caption = 'Password'
    end
    object Label3: TLabel
      Left = 8
      Top = 160
      Width = 28
      Height = 13
      Caption = 'From:'
    end
    object Label6: TLabel
      Left = 8
      Top = 261
      Width = 40
      Height = 13
      Caption = 'Subject:'
    end
    object Label4: TLabel
      Left = 8
      Top = 200
      Width = 16
      Height = 13
      Caption = 'To:'
    end
    object edtHost: TEdit
      Left = 8
      Top = 24
      Width = 287
      Height = 21
      TabOrder = 0
    end
    object edtPassword: TEdit
      Left = 216
      Top = 64
      Width = 199
      Height = 21
      PasswordChar = '*'
      TabOrder = 1
    end
    object edtUsername: TEdit
      Left = 8
      Top = 64
      Width = 199
      Height = 21
      TabOrder = 2
    end
    object cmbPort: TComboBox
      Left = 304
      Top = 24
      Width = 111
      Height = 21
      TabOrder = 3
      Items.Strings = (
        '25'
        '587'
        '465')
    end
    object rgSecurity: TRadioGroup
      Left = 8
      Top = 96
      Width = 407
      Height = 49
      Caption = '  Security  '
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'No TLS'
        'Implicit TLS'
        'Require TLS'
        'Explicit TLS')
      TabOrder = 4
    end
    object edtFrom: TEdit
      Left = 8
      Top = 176
      Width = 402
      Height = 21
      TabOrder = 5
    end
    object edtSubject: TEdit
      Left = 8
      Top = 277
      Width = 316
      Height = 21
      TabOrder = 6
    end
    object edtTo: TEdit
      Left = 8
      Top = 216
      Width = 402
      Height = 21
      TabOrder = 7
    end
    object memMessage: TMemo
      Left = 8
      Top = 304
      Width = 402
      Height = 257
      TabOrder = 8
    end
    object Button1: TButton
      Left = 330
      Top = 273
      Width = 75
      Height = 25
      Action = acSend
      TabOrder = 9
    end
  end
  object memLog: TMemo
    AlignWithMargins = True
    Left = 431
    Top = 3
    Width = 574
    Height = 574
    Align = alClient
    BorderStyle = bsNone
    Color = clBlack
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -16
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    ExplicitLeft = 428
    ExplicitTop = 0
    ExplicitWidth = 580
    ExplicitHeight = 626
  end
  object SMTP: TIdSMTP
    OnStatus = SMTPStatus
    IOHandler = SSL
    OnDisconnected = SMTPDisconnected
    OnConnected = SMTPConnected
    OnFailedRecipient = SMTPFailedRecipient
    SASLMechanisms = <>
    OnTLSNotAvailable = SMTPTLSNotAvailable
    Left = 155
    Top = 315
  end
  object Actions: TActionList
    Left = 199
    Top = 315
    object acSend: TAction
      Caption = 'Send'
      OnExecute = acSendExecute
    end
  end
  object SSL: TIdSSLIOHandlerSocketOpenSSL
    OnStatus = SSLStatus
    Destination = ':25'
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    OnStatusInfo = SSLStatusInfo
    Left = 115
    Top = 315
  end
end
